#!/bin/bash

# File: start_container.sh
# Author(s): Gus Welter
# Created: Wed Feb 17 06:44:20 EST 2021 
# Description:
# Acknowledgements:
# Copyright (c) 2021 Carnegie Mellon University
# This code is subject to the license terms contained in the code repo.

docker pull registry.gitlab.com/sray/cmu-ta2:latest

mkdir output
docker run -i -t \
    --rm \
    --name ta2_container \
    --mount type=bind,source=/home/sray/gitlab/cmu-ta2/scripts,target=/scripts \
    --mount type=bind,source=/home/sray/gitlab/cmu-ta2/seed_datasets_current/196_autoMpg_MIN_METADATA,target=/input \
    --mount type=bind,source=/home/sray/gitlab/cmu-ta2/output,target=/output \
    --mount type=bind,source=/home/sray/gitlab/cmu-ta2/static,target=/static \
    -e D3MINPUTDIR=/input \
    -e D3MPROBLEMPATH=/input/TRAIN/problem_TRAIN/problemDoc.json \
    -e D3MOUTPUTDIR=/output  \
    -e D3MCPU=8 \
    -e D3MTIMEOUT=20 \
    -e D3MSTATICDIR="/static" \
    --entrypoint=/scripts/run.sh \
    registry.gitlab.com/sray/cmu-ta2:latest
